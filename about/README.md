# Vue2 Module Federation Demo

This example demos consumption of federated modules from a Webpack bundle. `layout` app depends on a component exposed by `home` app.

---

# Running Demo

Run `npm start home`



- REMOTE (home): [localhost:3501](http://localhost:3501/)