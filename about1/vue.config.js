const { ModuleFederationPlugin } = require("webpack").container;
const path = require('path')

const paths = {
    // Source files
    // Исходники
    src: path.resolve(__dirname, '../src'),

    // Production build files
    // Директория для файлов сборки
    build: path.resolve(__dirname, '../dist'),

    // Static files that get copied to build folder
    // Статические файлы, которые будут скопированы в директорию для файлов сборки
    public: path.resolve(__dirname, '../public'),
}


module.exports = {
    runtimeCompiler: true,
    configureWebpack: config => {
        if (process.env.NODE_ENV === 'production') {
            // изменение конфигурации для production...
        } else {
            // изменения для разработки...


        }
        return {


            devtool: "source-map",

            target: "web",

            // // Откуда начинается сборка
            // entry: [paths.src + '/main.js'],

            // publicPath: "auto",

            // Where webpack outputs the assets and bundles
            // Куда помещаются файлы сборки
            output: {
                //   path: paths.build,
                // filename: '[name].bundle.js',
                filename: 'js/[name].bundle.js',
                // publicPath: './',
                //   publicPath: "auto",
            },
            // Customize the webpack build process
            // Настройки            

            plugins: [

                new ModuleFederationPlugin({
                    name: "about",
                    filename: "aboutRemoteEntry.js",
                    remotes: {
                        about: "about@http://localhost:8083/aboutRemoteEntry.js",
                    },
                    exposes: {
                        "./About": "./src/components/About",
                    },
                }),
            ],
            // Spin up a server for quick development
            // Запуск сервера для разработки
            devServer: {
                historyApiFallback: true,
                // contentBase: paths.build,
                open: true,
                compress: true,
                hot: true,
                port: 8083,
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
                    "Access-Control-Allow-Headers":
                        "X-Requested-With, content-type, Authorization",
                },
            },
            resolve: {
                alias: {
                    vue$: "vue/dist/vue.runtime.esm.js",
                },
                extensions: ["*", ".js", ".vue", ".json"],
            }
        }

        // config => config.plugin('minify').use(BabiliWebpackPlugin),
    }
}